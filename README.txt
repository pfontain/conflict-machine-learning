Conflict Simulation with Machine Learning

Description
- In this project the author is trying to create a conflict strategy
 choice predictor based on contextual and demographic input. The motivation
 is to apply such a predictor in NPC interaction in games driven by real data.
- Currently the conflict data is also included in the project.
 Only data cleaning and reformatting has been implemented yet.

Instructions
- The following folder can be loaded as an eclipse project.
- To execute the data cleaning/process either run DataProcessor
 from the command line (add -d to save to database), or use the
 eclipse launchables available.
- Database saving: the implementation assumes that a mysql database
 is running with the following attributes
	+ URL: localhost:8889/conflict
	+ admin user: user
	+ admin user password: RB5HLjfrWwL9zDET
- database attributes will be moved to configuration file
	
Data
- Data is stored in the data folders. Each subfolder contains batches
 of data specific to a area.
- Reformatted and cleaned data is stored in data_out folder.
- In config.ini we specify which files and attributes should be considered.

Weka
- weka source code was integrated in the project due to a needed
 modification to the database saver. This modification was not
 possible to do with simple inheritance. Composition or a more partial
 inclusion of the source code should be tried.

Papers
For a discussion on modeling conflict in video games consult the following article:
Gomes, Paulo, and Arnav Jhala. "AI Authoring for Virtual Characters in Conflict."
Ninth Artificial Intelligence and Interactive Digital Entertainment Conference. 2013.

For a description of the data collected by crowdsourcing consult the following article:
Swanson, Reid, and Arnav Jhala. "Rich computational model of conflict for virtual characters."
Intelligent Virtual Agents. Springer Berlin Heidelberg, 2012.