package conflict.pfontain.soe.ucsc.edu;

import java.io.StringWriter;

import weka.core.Instance;
import weka.core.InstanceComparator;

/**
 * InstanceDifferenceComparator can generate a textual difference between two weka instances
 * 
 * @author paulogomes
 *
 */
public class InstanceDifferenceComparator extends InstanceComparator {
	
	private static final long serialVersionUID = 6271746586204953027L;

	public String difference(Instance inst1,Instance inst2){
		if(compare(inst1,inst2)==0)
			return "";
		
		StringWriter differenceWriter = new StringWriter();
		
		if(inst1.numAttributes() != inst2.numAttributes()){
			differenceWriter.write("> (# attributes: "+inst1.numAttributes()+")");
			differenceWriter.write(System.getProperty("line.separator"));
			differenceWriter.write("< (# attributes: "+inst2.numAttributes()+")");
			differenceWriter.write(System.getProperty("line.separator"));
			
			return differenceWriter.toString();
		}
		
		for(int iAttribute = 0; iAttribute < inst1.numAttributes(); iAttribute++){
			String inst1AttributeValue = inst1.stringValue(iAttribute);
			String inst2AttributeValue = inst2.stringValue(iAttribute);
			
			if(!inst1AttributeValue.equals(inst2AttributeValue)){
				differenceWriter.write("> "+inst1.attribute(iAttribute).name()+": "+inst1AttributeValue);
				differenceWriter.write(System.getProperty("line.separator"));
				differenceWriter.write("< "+inst2.attribute(iAttribute).name()+": "+inst2AttributeValue);
				differenceWriter.write(System.getProperty("line.separator"));
			}
		}
		
		return differenceWriter.toString();
	}
	
}
