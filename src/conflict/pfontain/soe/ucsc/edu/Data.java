package conflict.pfontain.soe.ucsc.edu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Data parses CVS files and generates Weka data instances.
 * 
 * @author paulogomes
 *
 */
public class Data {
	private static final String ATTRIBUTE_SEPARATOR = "\",\"";
	private static final char LINE_START_CHARACTER = '\"';
	private static final String INPUT_IGNORE_HEADER_ATTRIBUTES = "INPUT_IGNORE_HEADER_ATTRIBUTES";
	private static final String BREAK_LINE_REPLACEMENT = "<br>";
	private static final ReplacementMapping[] replacementMappingArray = { new ReplacementMapping(
			"\"\"", "(quotation_mark)") };
	
	// TODO: Extract to configuration or common class
	private static final String WORKER_ID_ATTRIBUTE_NAME = "WorkerId";

	private static class ReplacementMapping {
		public String inputText;
		public String outputText;

		public ReplacementMapping(String inputText, String outputText) {
			this.inputText = inputText;
			this.outputText = outputText;
		}
	}

	private static HashMap<String, String> replacementMapping = new HashMap<String, String>();
	static {
		for (int iReplacement = 0; iReplacement < replacementMappingArray.length; iReplacement++)
			replacementMapping.put(
					replacementMappingArray[iReplacement].inputText,
					replacementMappingArray[iReplacement].outputText);
	}

	private Set<String> attributeSet = new LinkedHashSet<String>();
	private HashMap<String, Set<String>> attributeValues = new LinkedHashMap<String, Set<String>>();
	private List<LinkedHashMap<String, String>> data = new LinkedList<LinkedHashMap<String, String>>();
	private Set<String> textAttributes;
	private boolean considerSpacesAsMissing = true;
	private boolean hasTextAttributes = false;

	private Configuration configuration;

	public Data(Configuration configuration, Set<String> textAttributes) {
		this.configuration = configuration;
		this.textAttributes = new HashSet<String>(textAttributes);
		this.hasTextAttributes = true;
	}

	public void loadCSVFiles(List<String> inputDataFilenames,
			String inputDataFolder) throws IOException {

		List<String> inputDataFilePaths = new LinkedList<String>();
		for (String filename : inputDataFilenames)
			inputDataFilePaths.add(inputDataFolder + filename);

		for (String inputDataFilePath : inputDataFilePaths)
			addFileData(inputDataFilePath);
	}

	public void addAttributes(Collection<String> attributes) {
		attributeSet.addAll(attributes);
	}

	public void addInstance(LinkedHashMap<String, String> instance) {
		data.add(instance);
	}

	public void addAttributeValue(String attribute, String value) {
		if (!attributeValues.containsKey(attribute))
			attributeValues.put(attribute, new HashSet<String>());

		attributeValues.get(attribute).add(value);
	}

	public void addFileData(String inputDataFilePath) throws IOException {
		BufferedReader inputDataFileReader;
		try {

			inputDataFileReader = new BufferedReader(new FileReader(
					inputDataFilePath));
		} catch (IOException e) {
			throw new IOException("unable to reach input file "
					+ inputDataFilePath);
		}

		if (!inputDataFileReader.ready()) {
			inputDataFileReader.close();
			throw new IOException("input file " + inputDataFilePath
					+ " is empty");
		}

		try {
			String dataFileFirstLine = inputDataFileReader.readLine();

			List<String> dataAttributes = headerParser(dataFileFirstLine);
			List<String> ignoreHeaderAttributes = configuration
					.getVector(INPUT_IGNORE_HEADER_ATTRIBUTES);
			dataAttributes.removeAll(ignoreHeaderAttributes);
			addAttributes(dataAttributes);

			while (inputDataFileReader.ready()) {
				List<String> dataFields = tokenParser(inputDataFileReader,
						dataAttributes.size());

				Iterator<String> dataAttributesIterator = dataAttributes
						.iterator();
				Iterator<String> dataFieldsIterator = dataFields.iterator();
				LinkedHashMap<String, String> dataInstance = new LinkedHashMap<String, String>();
				while (dataFieldsIterator.hasNext()) {
					String dataAttribute = dataAttributesIterator.next();
					String dataField = dataFieldsIterator.next();
					if (!(dataField.isEmpty() && considerSpacesAsMissing)) {
						dataInstance.put(dataAttribute, dataField);
						if (!(hasTextAttributes && textAttributes
								.contains(dataAttribute))) {
							addAttributeValue(dataAttribute, dataField);
						}
					}
				}
				addInstance(dataInstance);
			}

		} catch (IOException e) {
			inputDataFileReader.close();
			throw new IOException(
					"unable to correctly read line in input file "
							+ inputDataFilePath);
		}

		inputDataFileReader.close();
	}

	public Instances createInstances(String name) {
		FastVector attributeInfo = new FastVector(attributeSet.size());
		Iterator<String> attributeIterator = attributeSet.iterator();
		while (attributeIterator.hasNext()) {
			String attributeName = attributeIterator.next();

			Attribute attribute;
			if (hasTextAttributes && textAttributes.contains(attributeName)) {
				attribute = new Attribute(attributeName, (FastVector) null);
			} else {
				FastVector attributeValuesVector;
				if (this.attributeValues.containsKey(attributeName)) {
					Set<String> attributeValues = this.attributeValues
							.get(attributeName);
					attributeValuesVector = new FastVector(
							attributeValues.size());
					for (String attributeValue : attributeValues)
						attributeValuesVector.addElement(attributeValue);
				} else {
					attributeValuesVector = new FastVector();
				}
				attribute = new Attribute(attributeName, attributeValuesVector);
			}
			attributeInfo.addElement(attribute);
		}

		Instances instances = new Instances(name, attributeInfo, data.size());

		for (LinkedHashMap<String, String> instanceData : data) {
			Instance instance = new Instance(instances.numAttributes());
			Iterator<Entry<String, String>> instanceDataEntryIterator = instanceData
					.entrySet().iterator();
			instance.setDataset(instances);
			while (instanceDataEntryIterator.hasNext()) {
				Entry<String, String> instanceDataEntry = instanceDataEntryIterator
						.next();
				String attributeName = instanceDataEntry.getKey();
				Attribute attribute = instances.attribute(attributeName);
				instance.setValue(attribute, instanceDataEntry.getValue());
			}
			instances.add(instance);
		}
		
		// If it has the worker id attribute
		if(instances.attribute(WORKER_ID_ATTRIBUTE_NAME)!=null)
			instances = filterOutNonUniqueWorkerIdInstances(instances);

		return instances;
	}
	
	static Instances filterOutNonUniqueWorkerIdInstances(Instances instances){
		HashMap<String,Integer> attributeUniqueInstancePosition  = new HashMap<String,Integer>();
		Attribute attribute = instances.attribute(WORKER_ID_ATTRIBUTE_NAME);
		
		for (int i = 0; i < instances.numInstances(); i++) {
		    Instance instance = instances.instance(i);
		    String attributeId = instance.toString(attribute);
		    
		    // Attribute id repeated ?
		    if (attributeUniqueInstancePosition.containsKey(attributeId))
		    	attributeUniqueInstancePosition.put(attributeId,null);
			else
				attributeUniqueInstancePosition.put(attributeId,i);
		}
		
		// In order to maintain order we order by original position
		Collection<Integer> instancePositions = attributeUniqueInstancePosition.values();
		instancePositions.removeAll(Collections.singleton(null));
		List< Integer > positionsList = new ArrayList< Integer >( instancePositions );
		Collections.sort( positionsList );
		
		Instances filteredInstances = new Instances(instances,0);
		Iterator<Integer> positionIterator = positionsList.iterator();
		
		while(positionIterator.hasNext()){
			Integer position = positionIterator.next();
			filteredInstances.add(instances.instance(position));
		}
		
		return filteredInstances;
	}
	

	private static List<String> headerParser(String line) throws IOException {
		List<String> tokens = new LinkedList<String>();
		int tokenStart;
		char[] lineArray = line.toCharArray();

		int i = 0;
		if (lineArray[i] != LINE_START_CHARACTER)
			throw new IOException("line starts with : " + lineArray[i]);

		i++;
		tokenStart = i;
		String separator = ATTRIBUTE_SEPARATOR;
		while (i < lineArray.length) {
			if (line.substring(i).startsWith(separator)) {
				tokens.add(line.substring(tokenStart, i));
				i += separator.length();
				tokenStart = i;
			} else if (lineArray[i] == LINE_START_CHARACTER
					&& (i + 1) == lineArray.length) {
				tokens.add(line.substring(tokenStart, i));
				break;
			} else {
				i++;
			}
		}

		return tokens;
	}

	private static List<String> tokenParser(BufferedReader inputDataFileReader,
			int expectedNumberTokens) throws IOException {
		List<String> tokens = new LinkedList<String>();
		int numberReadTokens = 0;
		String currentToken = "";

		while (inputDataFileReader.ready()
				&& numberReadTokens < expectedNumberTokens) {
			String line = inputDataFileReader.readLine();

			if (line.isEmpty()) {
				currentToken += BREAK_LINE_REPLACEMENT;
				continue;
			}

			char[] lineArray = line.toCharArray();
			int i = 0;
			if (lineArray[i] == LINE_START_CHARACTER)
				i++;
			int tokenStart = i;

			while (i < lineArray.length) {
				if (line.substring(i).startsWith(ATTRIBUTE_SEPARATOR)) {
					currentToken += line.substring(tokenStart, i);
					tokens.add(currentToken);
					numberReadTokens++;
					i += ATTRIBUTE_SEPARATOR.length();
					currentToken = "";
					tokenStart = i;
				} else {
					String replacementPrefix = replacementPrefix(line
							.substring(i));

					if (replacementPrefix != null) {
						String replacement = replacementMapping
								.get(replacementPrefix);
						if (tokenStart < i) {
							currentToken += line.substring(tokenStart, i);
						}
						currentToken += replacement;
						i += replacementPrefix.length();
						tokenStart = i;
						
					} else if (lineArray[i] == LINE_START_CHARACTER
							&& (i + 1) == lineArray.length) {
						currentToken += line.substring(tokenStart, i);
						tokens.add(currentToken);
						numberReadTokens++;
						i++;
						
					} else {
						i++;
					}
				}
			}

			if (numberReadTokens < expectedNumberTokens){
				if(tokenStart < i){
					currentToken += line.substring(tokenStart, i);
				}
				currentToken += BREAK_LINE_REPLACEMENT;
			}
		}

		if (numberReadTokens < expectedNumberTokens)
			throw new IOException("not enough tokens");

		return tokens;
	}

	private static String replacementPrefix(String s) {
		for (Entry<String, String> replacementEntry : replacementMapping
				.entrySet()) {
			if (s.startsWith(replacementEntry.getKey())) {
				return replacementEntry.getKey();
			}
		}

		return null;
	}
}
