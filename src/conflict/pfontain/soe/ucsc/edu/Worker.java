package conflict.pfontain.soe.ucsc.edu;

import java.util.LinkedList;
import java.util.List;

import weka.core.Instance;

/**
 * Worker represents a crowdsource worker from the data gathered.
 * Each worker can have several assignements.
 * 
 * @author paulogomes
 *
 */
public class Worker {
	private String workerId;
	private List<String> assignments;
	private Instance demographicData;
	
	public Worker(String workerId, String initialAssignment,Instance demographicInstance) {
		
		this.workerId = workerId;
		this.assignments = new LinkedList<String>();
		this.assignments.add(initialAssignment);
		this.demographicData = demographicInstance;
	}
	
	public void addAssignment(String assignment){
		assignments.add(assignment);
	}

	public String toString(){
		String text = "";
		
		text += this.workerId + " ";
		text += "assig";
		for(String assignment: this.assignments)
			text += "("+assignment+")";
		text += "," + this.demographicData.toString();
		
		return text;
	}
}
