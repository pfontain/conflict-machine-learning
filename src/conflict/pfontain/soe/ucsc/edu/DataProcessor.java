package conflict.pfontain.soe.ucsc.edu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;
import weka.core.converters.DatabaseSaver;

/**
 * DataProcessor loads configurations, parses CSV files using Data, saves
 * reformatted data in CSV and ARFF files, saves same data in a database if
 * prompted.
 * 
 * @author paulogomes
 * 
 */
public class DataProcessor {
	private static final String DATABASE_TABLE_USER_PASSWORD = "RB5HLjfrWwL9zDET";
	private static final String DATABASE_TABLE_USER = "user";
	private static final String DATABASE_URL = "jdbc:mysql://localhost:8889/conflict";
	private static final String DATA_INPUT_SUBFOLDER_NAME_PERSONALITY = "DATA_INPUT_SUBFOLDER_NAME_PERSONALITY";
	private static final String DATA_OUTPUT_FOLDER_NAME = "DATA_OUTPUT_FOLDER_NAME";
	private static final String DEMOGRAPHICS_TEXT_ATTRIBUTES = "DEMOGRAPHICS_TEXT_ATTRIBUTES";
	private static final String SCENARIOS_TEXT_ATTRIBUTES = "SCENARIOS_TEXT_ATTRIBUTES";
	private static final String DATA_INPUT_SUBFOLDER_NAME_SCENARIOS = "DATA_INPUT_SUBFOLDER_NAME_SCENARIOS";
	private static final String DATA_INPUT_SUBFOLDER_NAME_DEMOGRAPHICS = "DATA_INPUT_SUBFOLDER_NAME_DEMOGRAPHICS";
	private static final String DATA_INPUT_FOLDER_NAME = "DATA_INPUT_FOLDER_NAME";
	private static final String SCENARIOS_FILES = "SCENARIOS_FILES";
	private static final String OUTPUT_SCENARIOS_NAME = "OUTPUT_SCENARIOS_NAME";
	private static final String DEMOGRAPHICS_WANTED_ATTRIBUTES = "DEMOGRAPHICS_WANTED_ATTRIBUTES";
	private static final String DEMOGRAPHICS_FILES = "DEMOGRAPHICS_FILES";
	private static final String OUTPUT_DEMOGRAPHICS_NAME = "OUTPUT_DEMOGRAPHICS_NAME";
	private final static String CSV_EXTENSION = "csv";
	private final static String ARFF_EXTENSION = "arff";
	private static final String ASSIGNMENT_ID_ATTRIBUTE_NAME = "AssignmentId";

	private static final String CONFIG_FILE = "config.ini";
	private static final String PERSONALITY_FILES = "PERSONALITY_FILES";
	private static final String OUTPUT_PERSONALITY_NAME = "OUTPUT_PERSONALITY_NAME";
	private static final String PERSONALITY_TEXT_ATTRIBUTES = "PERSONALITY_TEXT_ATTRIBUTES";
	private static final String SYNTAX_ERROR_MESSAGE = "Incorrect Syntax: DataProcessor [-d]";
	
	// TODO: Extract to configuration or common class
	private static final String WORKER_ID_ATTRIBUTE_NAME = "WorkerId";

	private Configuration configuration;
	private boolean isSavingDatabase;

	public String getDataOutputFolderPath() {
		return this.configuration.getIndividual(DATA_OUTPUT_FOLDER_NAME)
				+ File.separatorChar;
	}

	public DataProcessor(Configuration configuration, boolean isSavingDatabase) {
		this.isSavingDatabase = isSavingDatabase;
		this.configuration = configuration;
	}

	public static void main(String[] args) {
		DataProcessor dataProcessor;
		try {
			dataProcessor = loadSettings(args);
		} catch (IOException e) {
			System.err.println(e);
			return;
		} catch (Exception e) {
			System.err.println(e);
			return;
		}

		dataProcessor.process();
	}

	private static DataProcessor loadSettings(String[] args) throws Exception,
			FileNotFoundException, IOException {
		DataProcessor dataProcessor;

		boolean isSavingDatabase = false;
		if (args.length > 1) {
			throw new Exception(SYNTAX_ERROR_MESSAGE);
		}

		if (args.length == 1) {
			if (args[0].equals("-d")) {
				isSavingDatabase = true;
			} else {
				throw new Exception(SYNTAX_ERROR_MESSAGE);
			}
		}

		Configuration configuration = new Configuration(CONFIG_FILE);
		configuration.load();

		dataProcessor = new DataProcessor(configuration, isSavingDatabase);
		return dataProcessor;
	}

	private void process() {
		try {
			loadAndSaveDemographics();
			loadAndSaveScenarios();
			loadAndSavePersonality();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return;
		}
	}

	private void loadAndSavePersonality() throws IOException {
		String inputFilenamesTag = PERSONALITY_FILES;
		String outputFilenameTag = OUTPUT_PERSONALITY_NAME;
		String inputSubfolderNameTag = DATA_INPUT_SUBFOLDER_NAME_PERSONALITY;
		String inputTextAttributesTag = PERSONALITY_TEXT_ATTRIBUTES;

		loadAndSaveInstances(inputFilenamesTag, outputFilenameTag,
				inputSubfolderNameTag, inputTextAttributesTag);
	}

	private void loadAndSaveScenarios() throws IOException {
		String inputFilenamesTag = SCENARIOS_FILES;
		String outputFilenameTag = OUTPUT_SCENARIOS_NAME;
		String inputSubfolderNameTag = DATA_INPUT_SUBFOLDER_NAME_SCENARIOS;
		String inputTextAttributesTag = SCENARIOS_TEXT_ATTRIBUTES;

		loadAndSaveInstances(inputFilenamesTag, outputFilenameTag,
				inputSubfolderNameTag, inputTextAttributesTag);
	}

	private void loadAndSaveDemographics() throws IOException {
		String inputFilenamesTag = DEMOGRAPHICS_FILES;
		String outputFilenameTag = OUTPUT_DEMOGRAPHICS_NAME;
		String inputSubfolderNameTag = DATA_INPUT_SUBFOLDER_NAME_DEMOGRAPHICS;
		String inputTextAttributesTag = DEMOGRAPHICS_TEXT_ATTRIBUTES;

		loadAndSaveInstances(inputFilenamesTag, outputFilenameTag,
				inputSubfolderNameTag, inputTextAttributesTag);
	}

	private void loadAndSaveInstances(String inputFilenamesTag,
			String outputFilenameTag, String inputSubfolderNameTag,
			String inputTextAttributesTag) throws IOException {
		List<String> inputFilenames = configuration
				.getVector(inputFilenamesTag);
		String inputFolder = configuration
				.getIndividual(DATA_INPUT_FOLDER_NAME)
				+ File.separatorChar
				+ configuration.getIndividual(inputSubfolderNameTag)
				+ File.separatorChar;
		String outputFilename = configuration.getIndividual(outputFilenameTag);
		String outputFilePathWithoutExtension = getDataOutputFolderPath()
				+ configuration.getIndividual(outputFilenameTag);

		Data data;

		Set<String> inputTextAttributes = new HashSet<String>(
				configuration.getVector(inputTextAttributesTag));
		data = new Data(configuration, inputTextAttributes);

		data.loadCSVFiles(inputFilenames, inputFolder);
		Instances dataIntances = data.createInstances(outputFilename);
		saveInstances(dataIntances, outputFilePathWithoutExtension,
				outputFilename);
	}

	private void saveInstances(Instances dataInstances,
			String outputFilePathWithoutExtension, String outputFilename)
			throws IOException {

		saveARFF(dataInstances, outputFilePathWithoutExtension);
		saveCSV(dataInstances, outputFilePathWithoutExtension);
		if (isSavingDatabase) {
			saveDatabase(dataInstances, outputFilename);
		}
	}

	private void saveDatabase(Instances dataInstances,
			String outputFilePathWithoutExtension) {

		try {
			Statement statement = null;
			try {
				Connection connection = getConnection();

				String query = "DROP TABLE IF EXISTS "
						+ outputFilePathWithoutExtension;
				statement = connection.createStatement();
				statement.executeUpdate(query);

			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				if (statement != null) {
					statement.close();
				}
			}
		} catch (SQLException e) {
			System.out.println("Unable to clean up query");
		}

		DatabaseSaver databaseSaver;
		try {
			databaseSaver = new DatabaseSaver();
			databaseSaver.setUrl(DATABASE_URL);
			databaseSaver.setUser(DATABASE_TABLE_USER);
			databaseSaver.setPassword(DATABASE_TABLE_USER_PASSWORD);
			databaseSaver.setInstances(dataInstances);
			databaseSaver.setRelationForTableName(false);
			databaseSaver.setTableName(outputFilePathWithoutExtension);
			databaseSaver.connectToDatabase();
			databaseSaver.writeBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveCSV(Instances demographicsDataIntances,
			String outputFilePathWithoutExtension) throws IOException {
		CSVSaver saverCSV = new CSVSaver();
		saverCSV.setInstances(demographicsDataIntances);
		saverCSV.setFile(new File(outputFilePathWithoutExtension + "."
				+ CSV_EXTENSION));
		saverCSV.writeBatch();
	}

	private void saveARFF(Instances demographicsDataIntances,
			String outputFilePathWithoutExtension) throws IOException {
		ArffSaver saverARFF = new ArffSaver();
		saverARFF.setInstances(demographicsDataIntances);
		saverARFF.setFile(new File(outputFilePathWithoutExtension + "."
				+ ARFF_EXTENSION));
		saverARFF.writeBatch();
	}

	public Connection getConnection() throws SQLException {

		Connection connection = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", DATABASE_TABLE_USER);
		connectionProps.put("password", DATABASE_TABLE_USER_PASSWORD);

		connection = DriverManager.getConnection(DATABASE_URL, connectionProps);
		// System.out.println("Connected to database");
		return connection;
	}

	@SuppressWarnings("unused")
	private HashMap<String, Worker> generateAssignmentDemographics(
			Instances data) {
		HashMap<String, Worker> workers = new HashMap<String, Worker>();
		HashMap<String, Worker> assignments = new HashMap<String, Worker>();

		HashMap<String, LinkedList<Instance>> dataByWorkerId = new HashMap<String, LinkedList<Instance>>();
		@SuppressWarnings("unchecked")
		Enumeration<Instance> dataInstancesEnumeration = data
				.enumerateInstances();
		Attribute workerIdAttribute = data.attribute(WORKER_ID_ATTRIBUTE_NAME);
		Attribute assignmentIdAttribute = data
				.attribute(ASSIGNMENT_ID_ATTRIBUTE_NAME);
		while (dataInstancesEnumeration.hasMoreElements()) {
			Instance instance = dataInstancesEnumeration.nextElement();
			String workerId = instance.toString(workerIdAttribute);

			if (dataByWorkerId.containsKey(workerId)) {
				String instanceAssignmentId = instance
						.stringValue(assignmentIdAttribute);
				Worker worker = workers.get(workerId);
				worker.addAssignment(instanceAssignmentId);

				LinkedList<Instance> workerIdList = dataByWorkerId
						.get(workerId);
				Instance firstInstance = workerIdList.getFirst();
				if (!firstInstance.equals(instance)) {
					InstanceDifferenceComparator instanceDifferenceComparator = new InstanceDifferenceComparator();
				}

				workerIdList.add(instance);
				assignments.put(instanceAssignmentId, worker);
			} else {
				String instanceAssignmentId = instance
						.stringValue(assignmentIdAttribute);
				Instance demographicData = new Instance(instance);
				demographicData
						.deleteAttributeAt(assignmentIdAttribute.index());
				Worker worker = new Worker(workerId, instanceAssignmentId,
						demographicData);
				workers.put(workerId, worker);

				LinkedList<Instance> workerIdList = new LinkedList<Instance>();
				workerIdList.add(instance);
				dataByWorkerId.put(workerId, workerIdList);
				assignments.put(instanceAssignmentId, worker);
			}
		}

		return assignments;
	}

	@SuppressWarnings("unused")
	private void trimAttributes(Instances data) {
		List<String> wantedAttributesList = configuration
				.getVector(DEMOGRAPHICS_WANTED_ATTRIBUTES);

		int iAttribute = 0;
		while (iAttribute < data.numAttributes()) {
			String attributeName = data.attribute(iAttribute).name();
			if (!wantedAttributesList.contains(attributeName)) {
				data.deleteAttributeAt(iAttribute);
			} else {
				iAttribute++;
			}
		}

	}

	// private static String replaceMissingValues(String dataFileLine) {
	// String replacedLine;
	//
	// String oldMissingToken = "\"\"";
	// String newMissingToken = "\"" + WEKA_MISSING_VALUE + "\"";
	// replacedLine = dataFileLine.replace(oldMissingToken, newMissingToken);
	//
	// return replacedLine;
	// }

}
