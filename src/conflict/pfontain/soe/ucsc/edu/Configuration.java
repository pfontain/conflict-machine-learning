

package conflict.pfontain.soe.ucsc.edu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Configuration loads data configurations from a file to be used in the data processing.
 * 
 * @author paulogomes
 *
 */
public class Configuration {
	private BufferedReader fileReader;
	private String readingMode;
	private HashMap<String,String> individualConfigurations;
	private HashMap<String,List<String>> vectorConfigurations;
	
	private final static String DEFAULT_READING_MODE = "DEFAULT";
	
	public Configuration(String filePath) throws FileNotFoundException{
		this.fileReader = new BufferedReader(new FileReader(filePath));
		
		readingMode = DEFAULT_READING_MODE;
		individualConfigurations = new HashMap<String,String>();
		vectorConfigurations = new HashMap<String,List<String>>();
	}
	
	public void load() throws IOException{
		String line;		
		
		while(this.fileReader.ready()){
			line = this.fileReader.readLine();
			
			if(line.isEmpty()){
				readingMode = DEFAULT_READING_MODE;
				continue;
			}
			
			StringTokenizer fileLineTokenizer = new StringTokenizer(line, " ");
			
			if(fileLineTokenizer.countTokens() > 2)
				throw new IOException("too many tokens in line " + line);
			
			String label = fileLineTokenizer.nextToken();
			
			
			if(readingMode.equals(DEFAULT_READING_MODE) && fileLineTokenizer.countTokens() == 0){
				readingMode = label;
				List<String> configurationList = new LinkedList<String>();
				vectorConfigurations.put(label, configurationList);
				
			}else if(fileLineTokenizer.countTokens() == 1){
				String value = fileLineTokenizer.nextToken();				
				individualConfigurations.put(label, value);
				readingMode = DEFAULT_READING_MODE;
				
			}else if(!readingMode.equals(DEFAULT_READING_MODE)){
				List<String> vectorConfiguration = vectorConfigurations.get(readingMode);
				vectorConfiguration.add(label);
			}else {
				throw new IOException("unable to understand configuration line: " + line);
			}
		}
	}

	public String toString(){
		String text = "";
		
		text += "INDIVIDUAL[";
		Set<Entry<String,String>> individualConfigurationsEntries = this.individualConfigurations.entrySet();
		for(Entry<String,String> entry: individualConfigurationsEntries){
			text += " " + entry.getKey() + " ";
			text += entry.getValue();
		}
		text += "]";
		
		text += " VECTOR[";
		Set<Entry<String,List<String>>> vectorConfigurationsEntries = this.vectorConfigurations.entrySet();
		for(Entry<String,List<String>> entry: vectorConfigurationsEntries){
			text += " " + entry.getKey() + "(";
			List<String> valueList = entry.getValue();
			for(String value: valueList)
				text += " " + value;
			text += entry.getValue();
			
			text += ")";
		}
		text += "]";
		
		return text;
	}
	
	public boolean hasIndividual(String key){
		return individualConfigurations.containsKey(key);
	}
	
	public String getIndividual(String key){
		return individualConfigurations.get(key);
	}
	
	public boolean hasVector(String key){
		return vectorConfigurations.containsKey(key);
	}
	
	public List<String> getVector(String key){
		return vectorConfigurations.get(key);
	}
	
}
