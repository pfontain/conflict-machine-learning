DROP TABLE IF EXISTS workers_scenarios_merged;

CREATE TABLE workers_scenarios_merged
  AS
(SELECT
scenarios.WorkerId AS scenario_workerId,
workers_merged.*,
scenarios.AssignmentId AS scenarios_assignmentId,
scenarios.WorkerId AS scenarios_workerId,
scenarios.Input_ConflictType AS conflict_type,
scenarios.Input_Role AS role,
scenarios.Answer_ContinuedFriendshipDuration AS continued_friendship_duration,
scenarios.Answer_Emotion AS emotion,
scenarios.Answer_FamilialRelationship AS familial_relationship,
scenarios.Answer_FriendshipType AS friendship_type,
scenarios.Answer_Issues AS issues,
scenarios.Answer_PreviousFriendshipDuration AS previous_friendship_duration,
scenarios.Answer_SameSex AS same_sex,
scenarios.Answer_SchoolRelationship AS school_relationship,
scenarios.Answer_Severity AS severity,
scenarios.Answer_Sex AS sex_conflict,
scenarios.Answer_WorkRelationship AS work_relationship,
scenarios.Answer_Satisfaction AS safisfaction,
scenarios.Answer_TimeLapsed AS time_lapsed,
scenarios.Answer_Improvement AS improvement
FROM scenarios LEFT OUTER JOIN workers_merged
	ON scenarios.WorkerId = workers_merged.workerId);

SELECT * FROM workers_scenarios_merged;

SELECT * FROM workers_scenarios_merged WHERE workerId IS NOT NULL;