DROP TABLE IF EXISTS workers_merged;

CREATE TABLE workers_merged
  AS
(SELECT
demographics.WorkerId AS workerId,
demographics.Answer_q1 AS demographic_q1,
demographics.Answer_q2 AS demographic_q2,
demographics.Answer_q3 AS demographic_q3,
demographics.Answer_q4 AS demographic_q4,
demographics.Answer_q5 AS demographic_q5,
demographics.Answer_q6 AS demographic_q6,
demographics.Answer_q7 AS demographic_q7,
demographics.Answer_q8 AS demographic_q8,
demographics.Answer_q9 AS demographic_q9,
demographics.Answer_q10 AS demographic_q10,
personality.Answer_q1 AS personality_q1,
personality.Answer_q2 AS personality_q2,
personality.Answer_q3 AS personality_q3,
personality.Answer_q4 AS personality_q4,
personality.Answer_q5 AS personality_q5,
personality.Answer_q6 AS personality_q6,
personality.Answer_q7 AS personality_q7,
personality.Answer_q8 AS personality_q8,
personality.Answer_q9 AS personality_q9,
personality.Answer_q10 AS personality_q10
FROM demographics LEFT OUTER JOIN personality
	ON demographics.WorkerId = personality.workerId);

SELECT * FROM workers_merged
