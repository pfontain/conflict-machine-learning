SELECT demographic_q4, Count(*) AS frequency
FROM workers_merged
GROUP BY demographic_q4
ORDER BY Count(*) DESC , demographic_q4;

demographic_q4,frequency
Graduated high school,162
Graduated college,111
A graduate degree other than an M.D. or Ph.D.,34
Never graduated high school,7
An M.D. or Ph.D.,4
NULL,1

SELECT demographic_q9, Count(*) AS frequency
FROM workers_merged
GROUP BY demographic_q9
ORDER BY Count(*) DESC , demographic_q9;

demographic_q9,frequency
0,197
1,44
2,32
3,16
4,4
5+,22
NULL 4
